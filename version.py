from __future__ import unicode_literals

__version__ = '0.5'
__release_date__ = '06.04.2017'

# Version 0.5:
# Release Date: 06.04.2017
# with query and sampler to query the database, and provide learning sets

# Version 0.2:
# Release Date: 10.01.2017
# with var_def, var_lib, read and write modules, one can collect grid data into ascii files

# Version 0.1:
# Release Date: 04.01.2017
# Developing the var_def, var_lib, insert_def and insert_lib modules

# Version 0.0:
# Release Date: 03.01.2017
# Designing the module, creating the __init__.py, setup.py, version.py and other starting module files